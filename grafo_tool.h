/*******************************************************************
  APO semestral project

  @Author Vojtěch Sýkora
  @Author Miroslav Falcmann
  
  (C) Copyright 2021 - 2021 by Miroslav Falcmann & Vojtěch Sýkora
      e-mail:   falcmmir@fel.cvut.cz             & sykorvo1@fel.cvut.cz
      license:  MIT

  grafo.c      - main file

 *******************************************************************/

#ifndef __GRAFO_TOOL__
#define __GRAFO_TOOL__

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "rgb.h"
#include "drawing.h"
#include "graphics.h"
#include "convert.h"
#include "position.h"

/*
 * Empties a char buffer
 * (Fills it up with NULLs)
 */
void empty_coordinates_buffer(char *buffer);

/*
 * Displays the current x coordinate
 * Led line shines red while it is redrawing the coordinate 
 * to indicate to the user that drawing is disabled for a second.
 */
void display_current_x_coordinates(char *position_string, int position_number);

/*
 * Displays the current y coordinate
 * Led line shines red while it is redrawing the coordinate 
 * to indicate to the user that drawing is disabled for a second.
 */
void display_current_y_coordinates(char *position_string, int position_number);

/*
 * Performs all necessary actions connected with the left (red) knob.
 * Changes the current position (X-coordinate), draws horizontal line.
 * X coordinates are defined in the range of LCD display width (0 - 479).
 */ 
void h_knob_action(int16_t *old_x_position, int16_t *curr_x_position, int16_t curr_y_position, uint8_t *old_h_knob, uint8_t *h_knob, uint32_t rgb_knobs_value, unsigned short selected_color);
/*
 * Performs all necessary actions connected with the right (blue) knob.
 * Changes the current position (Y-coordinate), draws vertical line.
 * Y coordinates are defined in the range of LCD display width (0 - 319).
 */ 
void v_knob_action(int16_t *old_y_position, int16_t curr_x_position, int16_t *curr_y_position, uint8_t *old_v_knob, uint8_t *v_knob, uint32_t rgb_knobs_value, unsigned short selected_color);

/*
 * Scans the current values of all 3 knobs in infinite loop.
 * If user rotates with a knob (changes the value the knob represents), 
 * all necessary tasks connected with this event are performed.
 */ 
void grafo_tool();

#endif
