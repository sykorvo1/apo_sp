#include "minion.h"

void fill_minion_grid(uint8_t grid_x, uint8_t grid_y, unsigned short color)
{
    draw_square(
        GRID_X_0 + GRID_SQUARE_SIZE * grid_x,
        GRID_Y_0 - GRID_SQUARE_SIZE * grid_y,
        GRID_X_0 + GRID_SQUARE_SIZE * grid_x + GRID_SQUARE_SIZE,
        GRID_Y_0 - GRID_SQUARE_SIZE * grid_y + GRID_SQUARE_SIZE,
        color);
}

void draw_minion()
{
    // Y 0
    fill_minion_grid(0, 0, COLOR_WHITE);
    fill_minion_grid(1, 0, COLOR_WHITE);
    fill_minion_grid(2, 0, COLOR_BROWN);
    fill_minion_grid(3, 0, COLOR_BROWN);
    fill_minion_grid(4, 0, COLOR_BROWN);
    fill_minion_grid(5, 0, COLOR_WHITE);
    fill_minion_grid(6, 0, COLOR_WHITE);
    fill_minion_grid(7, 0, COLOR_BROWN);
    fill_minion_grid(8, 0, COLOR_BROWN);
    fill_minion_grid(9, 0, COLOR_BROWN);
    fill_minion_grid(10, 0, COLOR_WHITE);
    fill_minion_grid(11, 0, COLOR_WHITE);

    // Y 1
    fill_minion_grid(0, 1, COLOR_WHITE);
    fill_minion_grid(1, 1, COLOR_WHITE);
    fill_minion_grid(2, 1, COLOR_WHITE);
    fill_minion_grid(3, 1, COLOR_BROWN);
    fill_minion_grid(4, 1, COLOR_BROWN);
    fill_minion_grid(5, 1, COLOR_WHITE);
    fill_minion_grid(6, 1, COLOR_WHITE);
    fill_minion_grid(7, 1, COLOR_BROWN);
    fill_minion_grid(8, 1, COLOR_BROWN);
    fill_minion_grid(9, 1, COLOR_WHITE);
    fill_minion_grid(10, 1, COLOR_WHITE);
    fill_minion_grid(11, 1, COLOR_WHITE);

    // Y 2
    fill_minion_grid(0, 2, COLOR_WHITE);
    fill_minion_grid(1, 2, COLOR_BROWN);
    fill_minion_grid(2, 2, COLOR_BLUE);
    fill_minion_grid(3, 2, COLOR_BLUE);
    fill_minion_grid(4, 2, COLOR_BLUE);
    fill_minion_grid(5, 2, COLOR_BLUE);
    fill_minion_grid(6, 2, COLOR_BLUE);
    fill_minion_grid(7, 2, COLOR_BLUE);
    fill_minion_grid(8, 2, COLOR_BLUE);
    fill_minion_grid(9, 2, COLOR_BLUE);
    fill_minion_grid(10, 2, COLOR_BROWN);
    fill_minion_grid(11, 2, COLOR_WHITE);

    // Y 3
    fill_minion_grid(0, 3, COLOR_BROWN);
    fill_minion_grid(1, 3, COLOR_BROWN);
    fill_minion_grid(2, 3, COLOR_BLUE);
    fill_minion_grid(3, 3, COLOR_BLUE);
    fill_minion_grid(4, 3, COLOR_BLUE);
    fill_minion_grid(5, 3, COLOR_BLUE);
    fill_minion_grid(6, 3, COLOR_BLUE);
    fill_minion_grid(7, 3, COLOR_BLUE);
    fill_minion_grid(8, 3, COLOR_BLUE);
    fill_minion_grid(9, 3, COLOR_BLUE);
    fill_minion_grid(10, 3, COLOR_BROWN);
    fill_minion_grid(11, 3, COLOR_BROWN);

    // Y 4
    fill_minion_grid(0, 4, COLOR_WHITE);
    fill_minion_grid(1, 4, COLOR_YELLOW);
    fill_minion_grid(2, 4, COLOR_BLUE);
    fill_minion_grid(3, 4, COLOR_BLUE);
    fill_minion_grid(4, 4, COLOR_BLUE);
    fill_minion_grid(5, 4, COLOR_BLUE);
    fill_minion_grid(6, 4, COLOR_BLUE);
    fill_minion_grid(7, 4, COLOR_BLUE);
    fill_minion_grid(8, 4, COLOR_BLUE);
    fill_minion_grid(9, 4, COLOR_BLUE);
    fill_minion_grid(10, 4, COLOR_YELLOW);
    fill_minion_grid(11, 4, COLOR_WHITE);

    // Y 5
    fill_minion_grid(0, 5, COLOR_WHITE);
    fill_minion_grid(1, 5, COLOR_YELLOW);
    fill_minion_grid(2, 5, COLOR_YELLOW);
    fill_minion_grid(3, 5, COLOR_BLUE);
    fill_minion_grid(4, 5, COLOR_BLUE);
    fill_minion_grid(5, 5, COLOR_BLUE);
    fill_minion_grid(6, 5, COLOR_BLUE);
    fill_minion_grid(7, 5, COLOR_BLUE);
    fill_minion_grid(8, 5, COLOR_BLUE);
    fill_minion_grid(9, 5, COLOR_YELLOW);
    fill_minion_grid(10, 5, COLOR_YELLOW);
    fill_minion_grid(11, 5, COLOR_WHITE);

    // Y 6
    fill_minion_grid(0, 6, COLOR_WHITE);
    fill_minion_grid(1, 6, COLOR_YELLOW);
    fill_minion_grid(2, 6, COLOR_YELLOW);
    fill_minion_grid(3, 6, COLOR_BLUE);
    fill_minion_grid(4, 6, COLOR_BLUE);
    fill_minion_grid(5, 6, COLOR_BLUE);
    fill_minion_grid(6, 6, COLOR_BLUE);
    fill_minion_grid(7, 6, COLOR_BLUE);
    fill_minion_grid(8, 6, COLOR_BLUE);
    fill_minion_grid(9, 6, COLOR_YELLOW);
    fill_minion_grid(10, 6, COLOR_YELLOW);
    fill_minion_grid(11, 6, COLOR_WHITE);

    // Y 7
    fill_minion_grid(0, 7, COLOR_WHITE);
    fill_minion_grid(1, 7, COLOR_YELLOW);
    fill_minion_grid(2, 7, COLOR_BLUE);
    fill_minion_grid(3, 7, COLOR_BLUE);
    fill_minion_grid(4, 7, COLOR_BLUE);
    fill_minion_grid(5, 7, COLOR_BLUE);
    fill_minion_grid(6, 7, COLOR_BLUE);
    fill_minion_grid(7, 7, COLOR_BLUE);
    fill_minion_grid(8, 7, COLOR_BLUE);
    fill_minion_grid(9, 7, COLOR_BLUE);
    fill_minion_grid(10, 7, COLOR_YELLOW);
    fill_minion_grid(11, 7, COLOR_WHITE);

    // Y 8
    fill_minion_grid(0, 8, COLOR_WHITE);
    fill_minion_grid(1, 8, COLOR_BLUE);
    fill_minion_grid(2, 8, COLOR_YELLOW);
    fill_minion_grid(3, 8, COLOR_YELLOW);
    fill_minion_grid(4, 8, COLOR_YELLOW);
    fill_minion_grid(5, 8, COLOR_YELLOW);
    fill_minion_grid(6, 8, COLOR_YELLOW);
    fill_minion_grid(7, 8, COLOR_YELLOW);
    fill_minion_grid(8, 8, COLOR_YELLOW);
    fill_minion_grid(9, 8, COLOR_YELLOW);
    fill_minion_grid(10, 8, COLOR_BLUE);
    fill_minion_grid(11, 8, COLOR_WHITE);

    // Y 9
    fill_minion_grid(0, 9, COLOR_WHITE);
    fill_minion_grid(1, 9, COLOR_YELLOW);
    fill_minion_grid(2, 9, COLOR_YELLOW);
    fill_minion_grid(3, 9, COLOR_YELLOW);
    fill_minion_grid(4, 9, COLOR_YELLOW);
    fill_minion_grid(5, 9, COLOR_YELLOW);
    fill_minion_grid(6, 9, COLOR_YELLOW);
    fill_minion_grid(7, 9, COLOR_YELLOW);
    fill_minion_grid(8, 9, COLOR_YELLOW);
    fill_minion_grid(9, 9, COLOR_YELLOW);
    fill_minion_grid(10, 9, COLOR_YELLOW);
    fill_minion_grid(11, 9, COLOR_WHITE);

    // Y 10
    fill_minion_grid(0, 10, COLOR_WHITE);
    fill_minion_grid(1, 10, COLOR_YELLOW);
    fill_minion_grid(2, 10, COLOR_YELLOW);
    fill_minion_grid(3, 10, COLOR_YELLOW);
    fill_minion_grid(4, 10, COLOR_BROWN);
    fill_minion_grid(5, 10, COLOR_BROWN);
    fill_minion_grid(6, 10, COLOR_BROWN);
    fill_minion_grid(7, 10, COLOR_BROWN);
    fill_minion_grid(8, 10, COLOR_YELLOW);
    fill_minion_grid(9, 10, COLOR_YELLOW);
    fill_minion_grid(10, 10, COLOR_YELLOW);
    fill_minion_grid(11, 10, COLOR_WHITE);

    // Y 11
    fill_minion_grid(0, 11, COLOR_WHITE);
    fill_minion_grid(1, 11, COLOR_YELLOW);
    fill_minion_grid(2, 11, COLOR_YELLOW);
    fill_minion_grid(3, 11, COLOR_BROWN);
    fill_minion_grid(4, 11, COLOR_YELLOW);
    fill_minion_grid(5, 11, COLOR_YELLOW);
    fill_minion_grid(6, 11, COLOR_YELLOW);
    fill_minion_grid(7, 11, COLOR_YELLOW);
    fill_minion_grid(8, 11, COLOR_BROWN);
    fill_minion_grid(9, 11, COLOR_YELLOW);
    fill_minion_grid(10, 11, COLOR_YELLOW);
    fill_minion_grid(11, 11, COLOR_WHITE);

    // Y 12
    fill_minion_grid(0, 12, COLOR_WHITE);
    fill_minion_grid(1, 12, COLOR_YELLOW);
    fill_minion_grid(2, 12, COLOR_YELLOW);
    fill_minion_grid(3, 12, COLOR_YELLOW);
    fill_minion_grid(4, 12, COLOR_YELLOW);
    fill_minion_grid(5, 12, COLOR_LIGHT_GRAY);
    fill_minion_grid(6, 12, COLOR_LIGHT_GRAY);
    fill_minion_grid(7, 12, COLOR_YELLOW);
    fill_minion_grid(8, 12, COLOR_YELLOW);
    fill_minion_grid(9, 12, COLOR_YELLOW);
    fill_minion_grid(10, 12, COLOR_YELLOW);
    fill_minion_grid(11, 12, COLOR_WHITE);

    // Y 13
    fill_minion_grid(0, 13, COLOR_WHITE);
    fill_minion_grid(1, 13, COLOR_YELLOW);
    fill_minion_grid(2, 13, COLOR_YELLOW);
    fill_minion_grid(3, 13, COLOR_YELLOW);
    fill_minion_grid(4, 13, COLOR_LIGHT_GRAY);
    fill_minion_grid(5, 13, COLOR_WHITE);
    fill_minion_grid(6, 13, COLOR_WHITE);
    fill_minion_grid(7, 13, COLOR_LIGHT_GRAY);
    fill_minion_grid(8, 13, COLOR_YELLOW);
    fill_minion_grid(9, 13, COLOR_YELLOW);
    fill_minion_grid(10, 13, COLOR_YELLOW);
    fill_minion_grid(11, 13, COLOR_WHITE);

    // Y 14
    fill_minion_grid(0, 14, COLOR_WHITE);
    fill_minion_grid(1, 14, COLOR_BLACK);
    fill_minion_grid(2, 14, COLOR_BLACK);
    fill_minion_grid(3, 14, COLOR_LIGHT_GRAY);
    fill_minion_grid(4, 14, COLOR_WHITE);
    fill_minion_grid(5, 14, COLOR_BLACK);
    fill_minion_grid(6, 14, COLOR_BLACK);
    fill_minion_grid(7, 14, COLOR_WHITE);
    fill_minion_grid(8, 14, COLOR_LIGHT_GRAY);
    fill_minion_grid(9, 14, COLOR_BLACK);
    fill_minion_grid(10, 14, COLOR_BLACK);
    fill_minion_grid(11, 14, COLOR_WHITE);

    // Y 15
    fill_minion_grid(0, 15, COLOR_WHITE);
    fill_minion_grid(1, 15, COLOR_BLACK);
    fill_minion_grid(2, 15, COLOR_BLACK);
    fill_minion_grid(3, 15, COLOR_LIGHT_GRAY);
    fill_minion_grid(4, 15, COLOR_WHITE);
    fill_minion_grid(5, 15, COLOR_BLACK);
    fill_minion_grid(6, 15, COLOR_BLACK);
    fill_minion_grid(7, 15, COLOR_WHITE);
    fill_minion_grid(8, 15, COLOR_LIGHT_GRAY);
    fill_minion_grid(9, 15, COLOR_BLACK);
    fill_minion_grid(10, 15, COLOR_BLACK);
    fill_minion_grid(11, 15, COLOR_WHITE);

    // Y 16
    fill_minion_grid(0, 16, COLOR_WHITE);
    fill_minion_grid(1, 16, COLOR_YELLOW);
    fill_minion_grid(2, 16, COLOR_YELLOW);
    fill_minion_grid(3, 16, COLOR_YELLOW);
    fill_minion_grid(4, 16, COLOR_LIGHT_GRAY);
    fill_minion_grid(5, 16, COLOR_WHITE);
    fill_minion_grid(6, 16, COLOR_WHITE);
    fill_minion_grid(7, 16, COLOR_LIGHT_GRAY);
    fill_minion_grid(8, 16, COLOR_YELLOW);
    fill_minion_grid(9, 16, COLOR_YELLOW);
    fill_minion_grid(10, 16, COLOR_YELLOW);
    fill_minion_grid(11, 16, COLOR_WHITE);

    // Y 17
    fill_minion_grid(0, 17, COLOR_WHITE);
    fill_minion_grid(1, 17, COLOR_WHITE);
    fill_minion_grid(2, 17, COLOR_YELLOW);
    fill_minion_grid(3, 17, COLOR_YELLOW);
    fill_minion_grid(4, 17, COLOR_YELLOW);
    fill_minion_grid(5, 17, COLOR_LIGHT_GRAY);
    fill_minion_grid(6, 17, COLOR_LIGHT_GRAY);
    fill_minion_grid(7, 17, COLOR_YELLOW);
    fill_minion_grid(8, 17, COLOR_YELLOW);
    fill_minion_grid(9, 17, COLOR_YELLOW);
    fill_minion_grid(10, 17, COLOR_WHITE);
    fill_minion_grid(11, 17, COLOR_WHITE);

    // Y 18
    fill_minion_grid(0, 18, COLOR_WHITE);
    fill_minion_grid(1, 18, COLOR_WHITE);
    fill_minion_grid(2, 18, COLOR_WHITE);
    fill_minion_grid(3, 18, COLOR_YELLOW);
    fill_minion_grid(4, 18, COLOR_YELLOW);
    fill_minion_grid(5, 18, COLOR_YELLOW);
    fill_minion_grid(6, 18, COLOR_YELLOW);
    fill_minion_grid(7, 18, COLOR_YELLOW);
    fill_minion_grid(8, 18, COLOR_YELLOW);
    fill_minion_grid(9, 18, COLOR_WHITE);
    fill_minion_grid(10, 18, COLOR_WHITE);
    fill_minion_grid(11, 18, COLOR_WHITE);
}
