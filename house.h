/*******************************************************************
  APO semestral project

  @Author Vojtěch Sýkora
  @Author Miroslav Falcmann
  
  (C) Copyright 2021 - 2021 by Miroslav Falcmann & Vojtěch Sýkora
      e-mail:   falcmmir@fel.cvut.cz             & sykorvo1@fel.cvut.cz
      license:  MIT

  grafo.c      - main file

 *******************************************************************/

#ifndef __PREMADE__
#define __PREMADE__

#include "definitions.h"
#include "drawing.h"

#define HALF_WIDTH WIDTH / 2 - 50
#define FLOOR_Y 300

#define AVERAGE(x, y) ((x) + (y)) / 2

/*
 * Draws a window sketch with a color
 */
void draw_window(int start_x, int start_y, int end_x, int end_y, unsigned short color);

/*
 * Draws a door sketch with a color
 */
void draw_door(unsigned short color);

/*
 * Draws a house sketch
 */
void draw_house();

#endif
