#include "house.h"

void draw_window(int start_x, int start_y, int end_x, int end_y, unsigned short color)
{
    draw_box(start_x, start_y, end_x, end_y, color);
    draw_line_horizontal(start_x, AVERAGE(start_y, end_y), end_x, color);
    draw_line_vertical(AVERAGE(start_x, end_x), start_y, end_y, color);
}

void draw_door(unsigned short color)
{
    draw_line_vertical(HALF_WIDTH - 10, FLOOR_Y, FLOOR_Y - 40, color);
    draw_line_vertical(HALF_WIDTH + 10, FLOOR_Y, FLOOR_Y - 40, color);
    draw_line_horizontal(HALF_WIDTH - 10, FLOOR_Y - 40, HALF_WIDTH + 10, color);
    draw_square(HALF_WIDTH - 7, FLOOR_Y - 22, HALF_WIDTH - 4, FLOOR_Y - 19, color);
}

void draw_house()
{

    // FRONT 2D
    draw_box(HALF_WIDTH - 50, FLOOR_Y, HALF_WIDTH + 50, FLOOR_Y - 100, COLOR_YELLOW);

    //ROOF 2D
    draw_diagonal_right_up(HALF_WIDTH - 50, FLOOR_Y - 100, 50, COLOR_RED);
    draw_diagonal_left_up(HALF_WIDTH + 50, FLOOR_Y - 100, 50, COLOR_RED);

    // DOOR
    draw_door(COLOR_ORANGE);

    //WINDOWS
    draw_window(HALF_WIDTH - 45, FLOOR_Y - 45, HALF_WIDTH - 15, FLOOR_Y - 75, COLOR_BLUE);
    draw_window(HALF_WIDTH + 15, FLOOR_Y - 45, HALF_WIDTH + 45, FLOOR_Y - 75, COLOR_BLUE);

    // 3D side
    draw_diagonal_right_up(HALF_WIDTH + 50, FLOOR_Y, 100, COLOR_YELLOW);
    draw_diagonal_right_up(HALF_WIDTH + 50, FLOOR_Y - 100, 100, COLOR_YELLOW);
    draw_line_vertical(HALF_WIDTH + 150, FLOOR_Y - 100, FLOOR_Y - 200, COLOR_YELLOW);
    draw_diagonal_right_up(HALF_WIDTH, FLOOR_Y - 150, 100, COLOR_RED);
    draw_diagonal_left_up(HALF_WIDTH + 150, FLOOR_Y - 200, 50, COLOR_RED);
}
