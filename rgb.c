#include "rgb.h"

uint32_t get_knobs_value()
{
  return *(volatile uint32_t *)(SPILED_REG_BASE + SPILED_REG_KNOBS_8BIT_o);
}

void store_to_led_line(uint32_t rgb_knobs_value)
{
  *(volatile uint32_t *)(SPILED_REG_BASE + SPILED_REG_LED_LINE_o) = rgb_knobs_value;
}

void store_to_rgb1(uint32_t rgb_knobs_value)
{
  *(volatile uint32_t *)(SPILED_REG_BASE + SPILED_REG_LED_RGB1_o) = rgb_knobs_value;
}

void store_to_rgb2(uint32_t rgb_knobs_value)
{
  *(volatile uint32_t *)(SPILED_REG_BASE + SPILED_REG_LED_RGB2_o) = rgb_knobs_value;
}

uint32_t set_color(uint8_t green_knob)
{
  unsigned short color_range = 31;
  if (green_knob < color_range)
  {
    store_to_rgb1(COLOR_BLACK_32);
    redraw_color_square(COLOR_BLACK);
    return COLOR_BLACK;
  }
  if (green_knob < color_range * 2)
  {
    store_to_rgb1(COLOR_RED_32);
    redraw_color_square(COLOR_RED);
    return COLOR_RED;
  }
  if (green_knob < color_range * 3)
  {
    store_to_rgb1(COLOR_GREEN_32);
    redraw_color_square(COLOR_GREEN);
    return COLOR_GREEN;
  }
  if (green_knob < color_range * 4)
  {
    store_to_rgb1(COLOR_BLUE_32);
    redraw_color_square(COLOR_BLUE);
    return COLOR_BLUE;
  }
  if (green_knob < color_range * 5)
  {
    store_to_rgb1(COLOR_ORANGE_32);
    redraw_color_square(COLOR_ORANGE);
    return COLOR_ORANGE;
  }
  if (green_knob < color_range * 6)
  {
    store_to_rgb1(COLOR_YELLOW_32);
    redraw_color_square(COLOR_YELLOW);
    return COLOR_YELLOW;
  }
  if (green_knob < color_range * 7)
  {
    store_to_rgb1(COLOR_PINK_32);
    redraw_color_square(COLOR_PINK);
    return COLOR_PINK;
  }
  else
  {
    store_to_rgb1(COLOR_WHITE_32);
    redraw_color_square(COLOR_WHITE);
    return COLOR_WHITE;
  }
}
