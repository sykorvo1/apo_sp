#include "grafo_tool.h"

void empty_coordinates_buffer(char *buffer)
{
  for (int i = 0; i < 3; i++)
  {
    buffer[i] = '\0';
  }
}

void display_current_x_coordinates(char *position_string, int position_number)
{
  store_to_led_line(COLOR_RED_32);
  empty_coordinates_buffer(position_string);
  IntegertoString(position_string, position_number);
  redraw_x_pos(position_string);
  store_to_led_line(COLOR_GREEN_32);
}

void display_current_y_coordinates(char *position_string, int position_number)
{
  store_to_led_line(COLOR_RED_32);
  empty_coordinates_buffer(position_string);
  IntegertoString(position_string, position_number);
  redraw_y_pos(position_string);
  store_to_led_line(COLOR_GREEN_32);
}

void h_knob_action(int16_t *old_x_position, int16_t *curr_x_position, int16_t curr_y_position, uint8_t *old_h_knob, uint8_t *h_knob, uint32_t rgb_knobs_value, unsigned short selected_color)
{
  set_current_position(curr_x_position, old_h_knob, *h_knob);
  if ((*curr_x_position) >= WIDTH)
  {
    (*curr_x_position) = WIDTH - 1;
  }
  else if ((*curr_x_position) < 0)
  {
    (*curr_x_position) = 0;
  }
  draw_line_horizontal(*old_x_position, curr_y_position, *curr_x_position, selected_color);
  (*old_x_position) = (*curr_x_position);
  (*h_knob) = rgb_knobs_value >> 16;
}

void v_knob_action(int16_t *old_y_position, int16_t curr_x_position, int16_t *curr_y_position, uint8_t *old_v_knob, uint8_t *v_knob, uint32_t rgb_knobs_value, unsigned short selected_color)
{
  set_current_position(curr_y_position, old_v_knob, *v_knob);
  if ((*curr_y_position) >= HEIGHT)
  {
    (*curr_y_position) = HEIGHT - 1;
  }
  else if ((*curr_y_position) < 30)
  {
    (*curr_y_position) = 30;
  }
  draw_line_vertical(curr_x_position, (*old_y_position), (*curr_y_position), selected_color);
  (*old_y_position) = (*curr_y_position);
  (*v_knob) = rgb_knobs_value;
}

void grafo_tool()
{
  store_to_led_line(COLOR_RED_32);
  draw_header();
  store_to_led_line(COLOR_GREEN_32);

  uint32_t rgb_knobs_value = get_knobs_value();

  uint8_t v_knob = rgb_knobs_value;
  uint8_t color_knob = rgb_knobs_value >> 8;
  uint8_t h_knob = rgb_knobs_value >> 16;
  unsigned short selected_color = set_color(color_knob);

  int16_t current_position_x = 0; // max 480
  uint8_t old_h_knob = h_knob;
  int16_t current_position_y = 30; // max 320
  uint8_t old_v_knob = v_knob;
  int16_t old_position_y = 30;
  int16_t old_position_x = 0;

  char x_pos[4] = "";
  char y_pos[4] = "";

  int loop_counter_h_knob = 0;
  int loop_counter_v_knob = 0;
  int x_coordinates_updated = 1;
  int y_coordinates_updated = 1;

  while (1)
  {

    rgb_knobs_value = get_knobs_value();

    // setting current color according to the middle knob
    if (color_knob != ((rgb_knobs_value & 0x0000ff00) >> 8))
    {
      color_knob = (rgb_knobs_value >> 8);
      store_to_led_line(COLOR_RED_32);
      selected_color = set_color(color_knob);
      store_to_led_line(COLOR_GREEN_32);
    }

    // setting current Y coordinates according to the right knob and drawing vertical line
    if (v_knob != (rgb_knobs_value & 0x000000ff))
    {
      v_knob_action(&old_position_y, current_position_x, &current_position_y, &old_v_knob, &v_knob, rgb_knobs_value, selected_color);
      loop_counter_v_knob = 0;
      y_coordinates_updated = 0;
    }

    // writing current Y coordinates on LCD screen
    loop_counter_v_knob++;
    if (!y_coordinates_updated && (loop_counter_v_knob >= MAX_LOOP_COUNT))
    {
      display_current_y_coordinates(y_pos, current_position_y);
      y_coordinates_updated = 1;
      loop_counter_v_knob = 0;
    }

    // setting current X coordinates according to the left knob and drawing horizontal line
    if (h_knob != ((rgb_knobs_value & 0x00ff0000) >> 16))
    {
      h_knob_action(&old_position_x, &current_position_x, current_position_y, &old_h_knob, &h_knob, rgb_knobs_value, selected_color);
      loop_counter_h_knob = 0;
      x_coordinates_updated = 0;
    }

    // writing current X coordinates on LCD screen
    loop_counter_h_knob++;
    if (!x_coordinates_updated && (loop_counter_h_knob >= MAX_LOOP_COUNT))
    {
      display_current_x_coordinates(x_pos, current_position_x);
      x_coordinates_updated = 1;
      loop_counter_h_knob = 0;
    }
  }
}
