/*******************************************************************
  APO semestral project

  @Author Vojtěch Sýkora
  @Author Miroslav Falcmann
  
  (C) Copyright 2021 - 2021 by Miroslav Falcmann & Vojtěch Sýkora
      e-mail:   falcmmir@fel.cvut.cz             & sykorvo1@fel.cvut.cz
      license:  MIT

  grafo.c      - main file

 *******************************************************************/

#ifndef __TOP_HEADER__
#define __TOP_HEADER__

#include "definitions.h"
#include "drawing.h"
#include "rgb.h"

#define TOP_COLOR_X 130
#define TOP_COLOR_Y 10

#define TOP_XCOORD_X 320
#define TOP_XCOORD_Y 7

#define TOP_YCOORD_X 400
#define TOP_YCOORD_Y 7

/*
 * Redraws the color square in the header 
 * indicating the currently selected color
 */
void redraw_color_square(unsigned short color);

/*
 * Redraws the x coordinate in the header 
 * indicating the current x position of the painter
 */
void redraw_x_pos(char *coordinate);

/*
 * Redraws the y coordinate in the header 
 * indicating the current y position of the painter
 */
void redraw_y_pos(char *coordinate);

/*
 * Draws the complete header with the dividing line to indicate where the user can draw
 */
void draw_header();

/*
 * Draws the intro message with the game name and manual
 * Also used to erase the manual faster with rewriting it using a black color
 */
void draw_intro(unsigned short color);

#endif
