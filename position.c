#include "position.h"

void set_current_position(int16_t *curr_pos, uint8_t *old_knob, uint8_t knob)
{
  if (knob - (*old_knob) > 200)
  { // user moved from 0 to 255
    *curr_pos -= (*old_knob) + 255 - knob;
  }
  else if (knob - (*old_knob) < -200)
  { // user moved from 255 to 0
    *curr_pos += 255 - (*old_knob) + knob;
  }
  else if (knob - (*old_knob) > 0)
  { // user moved to the left
    *curr_pos += knob - (*old_knob);
  }
  else if (knob - (*old_knob) < 0)
  { // user moved to the right
    *curr_pos += knob - (*old_knob);
  }
  (*old_knob) = knob;
}