#include "top_header.h"

void redraw_color_square(unsigned short color)
{
    draw_square(TOP_COLOR_X, TOP_COLOR_Y, TOP_COLOR_X + 10, TOP_COLOR_Y + 10, color);
}

void redraw_x_pos(char *coordinate)
{
    draw_word(coordinate, 3, TOP_XCOORD_X + 32, TOP_XCOORD_Y, COLOR_WHITE);
}

void redraw_y_pos(char *coordinate)
{
    draw_word(coordinate, 3, TOP_YCOORD_X + 32, TOP_YCOORD_Y, COLOR_WHITE);
}

void draw_header()
{
    draw_row(29, COLOR_WHITE);

    draw_word("Selected Color:", 15, 7, 7, COLOR_WHITE);

    draw_word("Drawing at:", 11, TOP_XCOORD_X - 100, TOP_XCOORD_Y, COLOR_WHITE);

    draw_word("X = 0", 5, TOP_XCOORD_X, TOP_XCOORD_Y, COLOR_WHITE);

    draw_word("Y = 30", 6, TOP_YCOORD_X, TOP_YCOORD_Y, COLOR_WHITE);
}
