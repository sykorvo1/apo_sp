/*******************************************************************
  APO semestral project

  @Author Vojtěch Sýkora
  @Author Miroslav Falcmann
  
  (C) Copyright 2021 - 2021 by Miroslav Falcmann & Vojtěch Sýkora
      e-mail:   falcmmir@fel.cvut.cz             & sykorvo1@fel.cvut.cz
      license:  MIT

  grafo.c      - main file

 *******************************************************************/

#ifndef __RGB__
#define __RGB__

#include <stdint.h>
#include "definitions.h"
#include "graphics.h"

/*
 * Access register holding 8 bit relative knobs position
 * The type "(volatile uint32_t*)" casts address obtained
 * as a sum of base address and register offset to the
 * pointer type which target in memory type is 32-bit unsigned
 * integer. The "volatile" keyword ensures that compiler
 * cannot reuse previously read value of the location.
 */
uint32_t get_knobs_value();

/* 
 * Store the read value to the register controlling individual LEDs 
 */
void store_to_led_line(uint32_t rgb_knobs_value);

/*
 * Store RGB knobs values to the corersponding components controlling
 * a color/brightness of the RGB LED 1
 */
void store_to_rgb1(uint32_t rgb_knobs_value);

/*
 * Store RGB knobs values to the corersponding components controlling
 * a color/brightness of the RGB LED 2
 */
void store_to_rgb2(uint32_t rgb_knobs_value);

/*
 * Sets the drawing color from the middle green knob
 * Stores the color to the RGB LED 1
 * Redraws the color in the top left part of the header which indicates the current color
 */
uint32_t set_color(uint8_t green_knob);

#endif
