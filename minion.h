/*******************************************************************
  APO semestral project

  @Author Vojtěch Sýkora
  @Author Miroslav Falcmann
  
  (C) Copyright 2021 - 2021 by Miroslav Falcmann & Vojtěch Sýkora
      e-mail:   falcmmir@fel.cvut.cz             & sykorvo1@fel.cvut.cz
      license:  MIT

  grafo.c      - main file

 *******************************************************************/

#ifndef __MINION__
#define __MINION__

#include "definitions.h"
#include "drawing.h"

#define GRID_SQUARE_SIZE 6
#define GRID_X_0 WIDTH / 2 - (6 * GRID_SQUARE_SIZE)
#define GRID_Y_0 HEIGHT - 10

/*
 * Used for drawing a minion using large squares in a grid style format
 */
void fill_minion_grid(uint8_t grid_x, uint8_t grid_y, unsigned short color);

/*
 * Draws a minion image
 * Drawing is done layer by layer for the best experience
 */
void draw_minion();

#endif
