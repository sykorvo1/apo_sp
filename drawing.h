/*******************************************************************
  APO semestral project

  @Author Vojtěch Sýkora
  @Author Miroslav Falcmann
  
  (C) Copyright 2021 - 2021 by Miroslav Falcmann & Vojtěch Sýkora
      e-mail:   falcmmir@fel.cvut.cz             & sykorvo1@fel.cvut.cz
      license:  MIT

  grafo.c      - main file

 *******************************************************************/

#ifndef __DRAWING__
#define __DRAWING__

#include "font_types.h"
#include "definitions.h"

/*
 * Sets the pixel on coords x,y with a color
 */
void set_pixel(int x, int y, unsigned short color);

/*
 * Draws a character font 8x16 on pixel (x,y) with a color
 * Redraws the whole rectangle (black color for empty space)
 */
void draw_char(int x, int y, char ch, unsigned short color);

/*
 * Draws a character font 8x16 on pixel (x,y) with a color
 * Redraws only the pixels with the character (faster)
 */
void draw_char_naked(int x, int y, char ch, unsigned short color);

/*
 * Draws a string font 8x16 starting on pixel (x,y) with a color
 * Redraws the whole rectangle (black color for empty space)
 */
void draw_word(const char *word, int word_len, int x, int y, unsigned short color);

/*
 * Draws a string font 8x16 starting on pixel (x,y) with a color
 * Redraws only the pixels with the characters (faster)
 */
void draw_word_naked(const char *word, int word_len, int x, int y, unsigned short color);

/*
 * Draws a full square from the starting x,y to the ending x,y position with a color
 */
void draw_square(int start_x, int start_y, int end_x, int end_y, unsigned short color);

/*
 * Draws a horizontal line on a whole row of pixels (over the whole screen)
 */
void draw_row(int row_y, unsigned short color);

/*
 * Draws a horizontal line using a color
 */
void draw_line_horizontal(int16_t old_position_x, int16_t old_position_y, int16_t current_position_x, unsigned short color);

/*
 * Draws a vertical line using a color
 */
void draw_line_vertical(int16_t old_position_x, int16_t old_position_y, int16_t current_position_y, unsigned short color);

/*
 * Draws a diagonal line in the right up direction using a color
 */
void draw_diagonal_right_up(uint16_t start_x, uint16_t start_y, uint16_t len, unsigned short color);

/*
 * Draws a diagonal line in the left up direction using a color
 */
void draw_diagonal_left_up(uint16_t start_x, uint16_t start_y, uint16_t len, unsigned short color);

/*
 * Draws only the borders of a rectangle
 */
void draw_box(int start_x, int start_y, int end_x, int end_y, unsigned short color);

#endif
