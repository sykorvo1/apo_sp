/*******************************************************************
  APO semestral project

  @Author Vojtěch Sýkora
  @Author Miroslav Falcmann
  
  (C) Copyright 2021 - 2021 by Miroslav Falcmann & Vojtěch Sýkora
      e-mail:   falcmmir@fel.cvut.cz             & sykorvo1@fel.cvut.cz
      license:  MIT

  grafo.c      - main file

 *******************************************************************/

#ifndef __TERMINAL__
#define __TERMINAL__

#include <stdint.h>
#include <stdbool.h>

#include "definitions.h"

void serp_write_reg(uint32_t reg, uint32_t val);

uint32_t serp_read_reg(uint32_t reg);

volatile uint32_t* rx_ready_init();

volatile uint32_t* tx_ready_init();

volatile uint32_t* rx_buff_init();

bool is_tx_ready();

bool is_rx_ready();

/*
 * Send single byte by UART port to a terminal, wait for ready
 * to send first.
 */
void serp_tx_byte(int data);

uint32_t serp_rx_byte();

/*
 * Write 32-bit hexadecimal number to the terminal.
 */
void serp_send_hex(unsigned int val);

#endif
