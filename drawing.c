#include "drawing.h"

void set_pixel(int x, int y, unsigned short color)
{
  ((volatile unsigned short *)(LCD_FB_START))[x + y * WIDTH] = color;
}

void draw_char(int x, int y, char ch, unsigned short color)
{
  int w = font_rom8x16.maxwidth;
  const font_bits_t *ptr;
  if ((ch >= font_rom8x16.firstchar) && (ch - font_rom8x16.firstchar < font_rom8x16.size))
  {
    if (font_rom8x16.offset)
    {
      ptr = &font_rom8x16.bits[font_rom8x16.offset[ch - font_rom8x16.firstchar]];
    }
    else
    {
      int bw = (font_rom8x16.maxwidth + 15) / 16;
      ptr = &font_rom8x16.bits[(ch - font_rom8x16.firstchar) * bw * font_rom8x16.height];
    }

    for (int i = 0; i < font_rom8x16.height; i++)
    {
      font_bits_t val = *ptr;
      for (int j = 0; j < w; j++)
      {
        if ((val & 0x8000) != 0)
        {
          set_pixel(x + j, y + i, color);
        }
        else
        {
          set_pixel(x + j, y + i, COLOR_BLACK);
        }
        val <<= 1;
      }
      ptr++;
    }
  }
}

void draw_char_naked(int x, int y, char ch, unsigned short color)
{
  int w = font_rom8x16.maxwidth;
  const font_bits_t *ptr;
  if ((ch >= font_rom8x16.firstchar) && (ch - font_rom8x16.firstchar < font_rom8x16.size))
  {
    if (font_rom8x16.offset)
    {
      ptr = &font_rom8x16.bits[font_rom8x16.offset[ch - font_rom8x16.firstchar]];
    }
    else
    {
      int bw = (font_rom8x16.maxwidth + 15) / 16;
      ptr = &font_rom8x16.bits[(ch - font_rom8x16.firstchar) * bw * font_rom8x16.height];
    }

    for (int i = 0; i < font_rom8x16.height; i++)
    {
      font_bits_t val = *ptr;
      for (int j = 0; j < w; j++)
      {
        if ((val & 0x8000) != 0)
        {
          set_pixel(x + j, y + i, color);
        }
        val <<= 1;
      }
      ptr++;
    }
  }
}

void draw_word(const char *word, int word_len, int x, int y, unsigned short color)
{
  for (int i = 0; i < word_len; i++)
  {
    draw_char(x + 8 * i, y, word[i], color);
  }
}

void draw_word_naked(const char *word, int word_len, int x, int y, unsigned short color)
{
  for (int i = 0; i < word_len; i++)
  {
    draw_char_naked(x + 8 * i, y, word[i], color);
  }
}

void draw_square(int start_x, int start_y, int end_x, int end_y, unsigned short color)
{
  for (int y = start_y; y < end_y; y++)
  {
    for (int x = start_x; x < end_x; x++)
    {
      set_pixel(x, y, color);
    }
  }
}

void draw_row(int row_y, unsigned short color)
{
  draw_square(0, row_y, WIDTH, row_y + 1, color);
}

void draw_line_horizontal(int16_t old_position_x, int16_t old_position_y, int16_t current_position_x, unsigned short color)
{
  if (current_position_x < old_position_x)
  {
    for (int x = old_position_x; x > current_position_x; x--)
    {
      set_pixel(x, old_position_y, color);
    }
  }
  else
  {
    for (int x = old_position_x; x < current_position_x; x++)
    {
      set_pixel(x, old_position_y, color);
    }
  }
}

void draw_line_vertical(int16_t old_position_x, int16_t old_position_y, int16_t current_position_y, unsigned short color)
{
  if (current_position_y < old_position_y)
  {
    for (int y = old_position_y; y > current_position_y; y--)
    {
      set_pixel(old_position_x, y, color);
    }
  }
  else
  {
    for (int y = old_position_y; y < current_position_y; y++)
    {
      set_pixel(old_position_x, y, color);
    }
  }
}

void draw_diagonal_right_up(uint16_t start_x, uint16_t start_y, uint16_t len, unsigned short color)
{
  for (int i = 0; i < len; i++)
  {
    set_pixel(start_x, start_y, color);
    start_x++;
    start_y--;
  }
}

void draw_diagonal_left_up(uint16_t start_x, uint16_t start_y, uint16_t len, unsigned short color)
{
  for (int i = 0; i < len; i++)
  {
    set_pixel(start_x, start_y, color);
    start_x--;
    start_y--;
  }
}

void draw_box(int start_x, int start_y, int end_x, int end_y, unsigned short color)
{
  draw_line_horizontal(start_x, start_y, end_x, color);
  draw_line_vertical(start_x, start_y, end_y, color);
  draw_line_vertical(end_x, start_y, end_y, color);
  draw_line_horizontal(start_x, end_y, end_x, color);
}


