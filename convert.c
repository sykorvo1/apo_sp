#include "convert.h"

void IntegertoString(char *string, int number)
{

  if (number == 0)
  {
    string[0] = '0';
    return;
  };
  int divide = 0;
  int modResult;
  int length = 0;
  int isNegative = 0;
  int copyOfNumber;
  copyOfNumber = number;
  if (number < 0)
  {
    isNegative = 1;
    number = 0 - number;
    length++;
  }
  while (copyOfNumber != 0)
  {
    length++;
    copyOfNumber /= 10;
  }

  for (divide = 0; divide < length; divide++)
  {
    modResult = number % 10;
    number = number / 10;
    string[length - (divide + 1)] = modResult + '0';
  }
  if (isNegative)
  {
    string[0] = '-';
  }
  string[length] = '\0';
}
