/*******************************************************************
  APO semestral project

  @Author Vojtěch Sýkora
  @Author Miroslav Falcmann
  
  (C) Copyright 2021 - 2021 by Miroslav Falcmann & Vojtěch Sýkora
      e-mail:   falcmmir@fel.cvut.cz             & sykorvo1@fel.cvut.cz
      license:  MIT

  grafo.c      - main file

 *******************************************************************/

#define _POSIX_C_SOURCE 200112L

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "definitions.h"
#include "drawing.h"
#include "terminal.h"
#include "house.h"
#include "minion.h"
#include "grafo_tool.h"

/*
 * Works with input from the terminal 
 * and chooses a background depeding on what user inputted
 */
int start_app()
{

  volatile uint32_t *rx_ready = rx_ready_init();
  volatile uint32_t *tx_ready = tx_ready_init();
  volatile uint32_t *rx_buff = rx_buff_init();

  while (1)
  {
    if ((*rx_ready & SERP_RX_ST_REG_READY_m) && *tx_ready)
    {
      uint32_t numb = *(rx_buff);
      serp_tx_byte('\n');
      serp_send_hex(numb);
      serp_tx_byte('\n');
      if (numb == 3 * 16 + 1)
      {
        // erase intro and draw on house background
        draw_intro(COLOR_BLACK);
        draw_house();
        grafo_tool();
        break;
      }
      else if (numb == 3 * 16 + 2)
      {
        // erase intro and draw on minion background
        draw_intro(COLOR_BLACK);
        draw_minion();
        grafo_tool();
        break;
      }
      else if (numb == 3 * 16 + 3)
      {
        // erase intro and draw on blank background
        draw_intro(COLOR_BLACK);
        grafo_tool();
        break;
      }
    }
  }
  return 0;
}

/*
 * MAIN
 */
int _start(int argc, char *argv[])
{
  draw_intro(COLOR_WHITE);
  start_app();
  return 0;
}
