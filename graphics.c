#include "graphics.h"

void redraw_color_square(unsigned short color)
{
    draw_square(TOP_COLOR_X, TOP_COLOR_Y, TOP_COLOR_X + 10, TOP_COLOR_Y + 10, color);
}

void redraw_x_pos(char *coordinate)
{
    draw_word(coordinate, 3, TOP_XCOORD_X + 32, TOP_XCOORD_Y, COLOR_WHITE);
}

void redraw_y_pos(char *coordinate)
{
    draw_word(coordinate, 3, TOP_YCOORD_X + 32, TOP_YCOORD_Y, COLOR_WHITE);
}

void draw_header()
{
    draw_row(29, COLOR_WHITE);

    draw_word("Selected Color:", 15, 7, 7, COLOR_WHITE);

    draw_word("Drawing at:", 11, TOP_XCOORD_X - 100, TOP_XCOORD_Y, COLOR_WHITE);

    draw_word("X = 0", 5, TOP_XCOORD_X, TOP_XCOORD_Y, COLOR_WHITE);

    draw_word("Y = 30", 6, TOP_YCOORD_X, TOP_YCOORD_Y, COLOR_WHITE);
}

void draw_intro(unsigned short color)
{
  int X_BULLET_POINTS = 50;
  int Y_BULLET_POINTS = 65;

  // TITLE
  if (color != COLOR_BLACK)
  {
    draw_word("GRAFO", 5, 220, 35, COLOR_GREEN);
    draw_box(216, 33, 220 + 5 * 8 + 2, 33 + 18, COLOR_GREEN);
  }
  else
  { // erase intro
    draw_word("GRAFO", 5, 220, 35, COLOR_BLACK);
    draw_box(216, 33, 220 + 5 * 8 + 2, 33 + 18, COLOR_BLACK);
  }

  draw_word_naked("- Open peripherals window in QtMips", 35, X_BULLET_POINTS, Y_BULLET_POINTS, color);
  draw_word_naked("- Start with choosing a background", 34, X_BULLET_POINTS, Y_BULLET_POINTS + 18, color);
  draw_word_naked("- Write one of these numbers to the terminal", 44, X_BULLET_POINTS, Y_BULLET_POINTS + 2 * 18, color);
  draw_word_naked("    - 1 for a House", 19, X_BULLET_POINTS, Y_BULLET_POINTS + 3 * 18, color);
  draw_word_naked("    - 2 for a Minion", 20, X_BULLET_POINTS, Y_BULLET_POINTS + 4 * 18, color);
  draw_word_naked("    - 3 for a blank background", 30, X_BULLET_POINTS, Y_BULLET_POINTS + 5 * 18, color);

  draw_word_naked("- Rotate the left (red) knob to draw horizontally", 49, X_BULLET_POINTS, Y_BULLET_POINTS + 6 * 18, color);
  draw_word_naked("- Rotate the right (blue) knob to draw vertically", 49, X_BULLET_POINTS, Y_BULLET_POINTS + 7 * 18, color);
  draw_word_naked("- Rotate the middle (green) knob to change color", 48, X_BULLET_POINTS, Y_BULLET_POINTS + 8 * 18, color);

  draw_word_naked("- Enjoy!", 8, X_BULLET_POINTS, Y_BULLET_POINTS + 9 * 18, color);
}
