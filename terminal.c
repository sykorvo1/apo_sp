#include "terminal.h"

void serp_write_reg(uint32_t reg, uint32_t val)
{
  *(volatile uint32_t *)(SERIAL_PORT_BASE + reg) = val;
}

uint32_t serp_read_reg(uint32_t reg)
{
  return *(volatile uint32_t *)(SERIAL_PORT_BASE + reg);
}

volatile uint32_t* rx_ready_init() {
    return (volatile uint32_t *)(SERIAL_PORT_BASE + SERP_RX_ST_REG_o);
}

volatile uint32_t* tx_ready_init() {
    return (volatile uint32_t *)(SERIAL_PORT_BASE + SERP_TX_ST_REG_o);
}

volatile uint32_t* rx_buff_init() {
    return (volatile uint32_t *)(SERIAL_PORT_BASE + SERP_RX_DATA_REG_o);
}

bool is_tx_ready() {
    return (serp_read_reg(SERP_TX_ST_REG_o) & SERP_TX_ST_REG_READY_m);
}

bool is_rx_ready() {
    return (serp_read_reg(SERP_RX_ST_REG_o) & SERP_RX_ST_REG_READY_m);
}

void serp_tx_byte(int data)
{
  while (!is_tx_ready());
  serp_write_reg(SERP_TX_DATA_REG_o, data);
}


uint32_t serp_rx_byte()
{
  while (!is_rx_ready());
  return *rx_buff_init();
}

void serp_send_hex(unsigned int val)
{
  int i;
  for (i = 8; i > 0; i--)
  {
    char c = (val >> 28) & 0xf;
    if (c < 10)
      c += '0';
    else
      c += 'A' - 10;
    serp_tx_byte(c);
    val <<= 4;
  }
}


