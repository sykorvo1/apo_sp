/*******************************************************************
  APO semestral project

  @Author Vojtěch Sýkora
  @Author Miroslav Falcmann
  
  (C) Copyright 2021 - 2021 by Miroslav Falcmann & Vojtěch Sýkora
      e-mail:   falcmmir@fel.cvut.cz             & sykorvo1@fel.cvut.cz
      license:  MIT

  grafo.c      - main file

 *******************************************************************/

#ifndef __POSITION__
#define __POSITION__

#include <stdint.h>

/*
 * Sets current position (either x or y)
 * Setting is based on the knob difference to decide in which direction and how far to draw
 */
void set_current_position(int16_t *curr_pos, uint8_t *old_knob, uint8_t knob);

#endif
